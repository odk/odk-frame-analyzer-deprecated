import os
import logging
import signal
import sys
import argparse

import simplejson as json
import asyncio
from odkframelib.Utils import save_file, privacy_check, get_model_version
from aio_pika import connect, Message, IncomingMessage, ExchangeType
from garbage_detection import GarbageImageClassifier
from frame import RawFrame

GarbageImageClassifier = GarbageImageClassifier()

SETTINGS = {
    'RMQ_USER': os.environ.get('RMQ_USER') or 'odk',
    'RMQ_PASSWORD': os.environ.get('RMQ_PASSWORD') or 'development',
    'RMQ_URL': os.environ.get('RMQ_URL') or 'localhost:5672',
    'RMQ_QUEUE_FRAMES_FOR_ML': 'queue_raw_frames',
    'RMQ_EXCHANGE_FRAMES_FOR_ML': 'exchange_raw_frames',
    'RMQ_QUEUE_ANALYSED_FRAMES': 'queue_analysed_frames',
    'RMQ_EXCHANGE_FRAMES_ANALYSED': 'exchange_analysed_frames',
    'ROUTING_KEY': 'frame',
}

def parse_args(arguments):
    parser = argparse.ArgumentParser()
    parser.add_argument('--privacy_filter_off', action='store_true', help='Privacy filter is turned on')
    parser.add_argument('--save_all', action='store_true', help='Also store frames without objects', default=False)

    args = parser.parse_args(arguments)
    return vars(args)


class MlWorker:

    def __init__(self, privacy_filter_off, save_all):
        self.rmq_conn = None
        self.channel = None
        self.setup_logger()
        self.ml_queue_name = None
        self.exchange_ml = None
        self.analysed_queue_name = None
        self.exchange_analysed = None
        self.is_ready = False
        self.privacy_filter_off = privacy_filter_off
        self.save_all = save_all

    def setup_logger(self):
        self.logger = logging.getLogger(__name__)

        if not self.logger.handlers:
            logging.basicConfig(
                level=logging.INFO,
                format='%(asctime)s %(name)s %(levelname)-4s %(message)s')

    async def setup_consumer(self, ml_queue_name: str):
        self.rmq_conn = await connect(
            "amqp://{0}:{1}@{2}".format(
                SETTINGS['RMQ_USER'],
                SETTINGS['RMQ_PASSWORD'],
                SETTINGS['RMQ_URL']),
            loop=asyncio.get_running_loop()
        )

        print("Connecting to RMQ at URL: {0}".format(SETTINGS['RMQ_URL']))

        self.channel = await self.rmq_conn.channel()

        self.ml_queue_name = ml_queue_name
        self.exchange_ml = await self.channel.declare_exchange(
            SETTINGS['RMQ_EXCHANGE_FRAMES_FOR_ML'], ExchangeType.DIRECT)
        queue_ml = await self.channel.declare_queue(self.ml_queue_name)
        await queue_ml.bind(self.exchange_ml)
        await queue_ml.consume(self.handle_new_frame_on_ml_queue, no_ack=True)

    async def setup_queue(self, analysed_queue_name: str):
        self.rmq_conn = await connect(
            "amqp://{0}:{1}@{2}".format(
                SETTINGS['RMQ_USER'],
                SETTINGS['RMQ_PASSWORD'],
                SETTINGS['RMQ_URL']
            ),
            loop=asyncio.get_running_loop()
        )

        self.analysed_queue_name = analysed_queue_name
        self.channel = await self.rmq_conn.channel()
        self.exchange_analysed = await self.channel.declare_exchange(
            SETTINGS['RMQ_EXCHANGE_FRAMES_ANALYSED'], ExchangeType.DIRECT)

        self.is_ready = True

    async def queue_analysed_frame(self, analysed_frame: RawFrame):
        if not self.is_ready:
            await self.setup_queue(SETTINGS["RMQ_QUEUE_ANALYSED_FRAMES"])

        await self.exchange_analysed.publish(
            message=Message(analysed_frame.json().encode('utf8')),
            routing_key=SETTINGS["ROUTING_KEY"],
        )

        print("Analysed frame queued")

    async def handle_new_frame_on_ml_queue(self, message: IncomingMessage):
        if not self.is_ready:
            await self.setup_queue(SETTINGS["RMQ_QUEUE_ANALYSED_FRAMES"])

        frame_data_dict = json.loads(message.body.decode("utf-8"))

        if type(frame_data_dict) is not dict:
            print("frame_data_dict not a dict")
            return

        frame = RawFrame(
            img=frame_data_dict['img'],
            taken_at=frame_data_dict['taken_at'],
            lat_lng=frame_data_dict['lat_lng'],
            stream_id=frame_data_dict['stream_id'],
            stream_meta=frame_data_dict['stream_meta'],
        )

        analysed_frame_data, processed_img = GarbageImageClassifier.detect_image(frame.img)

        if analysed_frame_data is None:
            print("Nothing detected")

            if self.save_all == True:
                if frame.stream_meta.get("user_type") != "demo":
                    save_file(frame, something_detected=False, privacy_filter_off=self.privacy_filter_off)
                else:
                    print("Not saving frame")
            return

        frame.img_meta = analysed_frame_data['frame_meta']
        frame.detected_objects = analysed_frame_data['detected_objects']
        frame.object_count = analysed_frame_data['counts']
        frame.analyser_meta = analysed_frame_data['analyser_meta']
        frame.processed_img = processed_img

        # Set model version based on creation date of model file
        frame.analyser_meta['model_version'] = get_model_version()

        print("Detected objects: ", frame.object_count)

        if privacy_check(frame.object_count):
            file_location = "not_saved"
            if frame.stream_meta.get("user_type") != "demo":
                file_location = save_file(frame, something_detected=True, privacy_filter_off=self.privacy_filter_off)
            else:
                print("Not saving frame, because user type is `demo`")

            frame.img_meta['file_location'] = file_location
        else:
            print("Not saving frame, because only privacy objects detected")

        # Discard image data
        frame.img = None
        frame.processed_img = None

        send_analysed_task = asyncio.create_task(
            self.queue_analysed_frame(frame))
        await send_analysed_task


if __name__ == "__main__":
    def signal_handler(signal, frame):
        print("Exiting...")
        sys.exit(0)


    args = parse_args(sys.argv[1:])
    # Handle exit through CTRL+C gracefully
    signal.signal(signal.SIGINT, signal_handler)

    loop = asyncio.get_event_loop()
    mlworker = MlWorker(**args)
    loop.create_task(mlworker.setup_consumer(SETTINGS["RMQ_QUEUE_FRAMES_FOR_ML"]))
    print(' [*] Waiting for frames. To exit press CTRL+C')
    loop.run_forever()
